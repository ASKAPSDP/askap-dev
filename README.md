# ASKAP Dev 
This directory contains utilities for ASKAP developers to help with developing, testing, building and deploying releases.

## config
Config files for ASKAP SDP including:

### module definition files
Module definition files (\*.moddef) or templates of these files marked up with substitution strings (\*.module-template) which can be set at deploy time to set version numbering etc. These files are for suitable deployment on galaxy using the [environment modules system](https://modules.readthedocs.io/en/latest/). They set up the necessary paths and can define aliases to make the scripts more command like.
* `askapcli.module-template` - ASKAP command line interface
* `askapcray.moddef` - Cray environment setup
* `askapdata.moddef` - Measures data
* `askapdev.moddef` - Developer module
* `askapenv.moddef` - Standard ASAKP environment setup module
* `askappy.moddef` - Standard ASAKP Python environment setup module
* `askappipeline.module-template` - ASKAP SDP pipeline
* `askapservices.module-template` - ASKAP SDP services
* `askapsoft.module-template` - ASKAP SDP imaging and calibration
* `askapsofthook.moddef` - Runtime lib intercept hook, put your lib first before the prod one (CAUTION)
* `askaputils.moddef` - ASKAP utilities
* `askapvis.moddef` - ASKAP visualisation
* `bbcp.moddef` - bbcp copy utility
* `default-version.template` - `.version` template file with version substitution string

## deploy
This contains tools for deploying ASKAP SDP components that are not deployed via ASKAP SDP repos.
* `acesops-deploy.sh` - Deploy the latest version of the ACES module (requires svn).
* `askappy-deploy.sh` - Deploy the ASKAP Python Env module
### askappy
```
askapdev>$ mkdir _install
askapdev>$ . deploy/askappy-deploy.sh _build _install
```
This will create a python virtual env called `askappy` in the `_install` dir as well as a `modulefiles/askappy` module defintition file for deploying to galaxy.
For now you will want to do this locally in your area and then copy the module to deploy because it is not versioned yet and will clobber the deployed version if `_install` is replaced by a production location.

## developer
This is for developer aids like file headers, code templates and generators.
* `Header.template` - code file header template boilerplate text
* `cookiecutter-templates` - cookie cutter template definitions (ala [cookiecutter](https://cookiecutter.readthedocs.io/en/latest/))

## tests
Some scripts for performing tests of the platform (not so much the code).
* `iotests` - testing I/O rates of the disks

## tools
Scripts etc for dev use. These can be access via the `askapdev` module.
* `devmodule.sh` - Use this to make deploying an arbitrary module, perhaps one in your own area, to a target modulefiles dir which is referenced by module use <modulefiles-dir>. See `devmodule -h` for help.

