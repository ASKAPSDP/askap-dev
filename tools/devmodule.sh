#!/bin/bash 

# devmodule.sh
#
# Install or uninstall a dev/test module with the given modulefile to the given modules dir.
# This is a general script for just copying a module file to a modulesfile dir -
# it does little in the way of checking although it does check for file and dir
# existence and also check for the prescence of the DEV_MARKER in the module file.
# This is added by the install if not there.
#
# This does not copy the module code anywhere, it merely installs the module file
# which needs to reference the actual code.
#
# @copyright (c) 2017 CSIRO
# Australia Telescope National Facility (ATNF)
# Commonwealth Scientific and Industrial Research Organisation (CSIRO)
# PO Box 76, Epping NSW 1710, Australia
# atnf-enquiries@csiro.au
#
# This file is part of the ASKAP software distribution.
#
# The ASKAP software distribution is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# @author Eric Bastholm <Eric.Bastholm@csiro.au>
#
main() {
  # remember the current dir
  CURRENT_DIR=`pwd`

  # find the location of our bin so we can source our common stuff - this is immune to install location
  ASKAPDEV_BIN_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  source ${ASKAPDEV_BIN_DIR}/askapdev-common.bash

  # Usage
  me=$(basename "$0")
  USAGE="$me [-h | ?] [-1 | -0] modulefile modulefilepath"

  INSTALL=true  #default
  MODULEFILE=""
  MODULEPATH=""
  MODULENAME=""

  # have to run as askapops to manipulate modules - this will cause exit if not
  #doUserCheck

  processCmdLine "$@" 

  if [ "$INSTALL" = true ]; then
      doInstall
  else
      doRemove
  fi

  cd ${CURRENT_DIR}
}

doInstall() {
  if  [ ! -e "${MODULEFILE}" ]; then
    printf "The given module file \"%s\" does not exist\n" "${MODULEFILE}"
    exit $ERROR_BAD_FILE_SPEC;
  fi
  if [ ! -f "${MODULEFILE}" ]; then
    printf "The given module file \"%s\" is not a file\n" "${MODULEFILE}"
    exit $ERROR_BAD_FILE_SPEC;
  fi

  if [ -e ${MODULEPATH}/${MODULENAME} ]; then
    printf "The modulefile, \"%s\", already exists - uninstall first.\n" "${MODULEPATH}/${MODULENAME}"
    exit $ERROR_OVERWRITE_CONFLICT
  fi

  if grep -q "${MODULES_COOKIE}" "${MODULEFILE}"; then
    printf "Installing \"%s\"\n" "${MODULEPATH}/${MODULENAME}"
    cp ${MODULEFILE} ${MODULEPATH}
  else
    printf "The given module file, \"%s\", is not recognised as a module file.\n" "${MODULEFILE}" 
    exit $ERROR_BAD_FILE_SPEC
  fi

  if ! grep -q "${DEV_MARKER}" "${MODULEPATH}/${MODULENAME}"; then
    echo "#${DEV_MARKER}" >> ${MODULEPATH}/${MODULENAME}
  fi
}

doRemove() {

  if [ ! -f ${MODULEPATH}/${MODULEFILE} ]; then
    printf "Cannot uninstall module, \"%s\", as it does not exist.\n" "${MODULEPATH}/${MODULEFILE}"
    exit $ERROR_PATH_DOES_NOT_EXIST
  fi

  # Check for DEV_MARKER to make sure we are only removing a dev module
  if grep -q "${DEV_MARKER}" "${MODULEPATH}/${MODULEFILE}"; then
    printf "uninstalling \"%s\"\n" "${MODULEPATH}/${MODULEFILE}"
    rm -f ${MODULEPATH}/${MODULEFILE}
  else
    printf "The given modulefile, \"%s\", is not a DEV module.\n" "${MODULEPATH}/${MODULEFILE}"
    exit $ERROR_BAD_FILE_SPEC
  fi
}

showHelp() {
  printf "%s \n" "$USAGE"
  printf "Install or uninstall a module file in the given area.\n"
  printf "    -h | ?            this help\n"
  printf "    -1                Install the given modulefile in the modulefilepath given\n"
  printf "    -0                Delete the given modulefile in the modulefilepath given\n"
  printf "\n"
}

# Process the command line parameters.
# Note: the logger has not been initialise yet so don't use log()
#
# Valid parameters:
#   -h | ?          Help
#   -1              Install the given modulefile in the modulefilepath given
#   -0              Delete the given modulefile in the modulefilepath given
#   modulefile      The modulefile to install
#   modulefilepath  The directory to put the modulefile
#
processCmdLine() {

  # For option processing
  OPTIND=1

  # Process our options
  while getopts "h?10" opt "$@"; do
      case "$opt" in
      
      h|\?)
          showHelp
          exit $NO_ERROR
          ;;
      1)
          INSTALL=true
          ;;
      0)
          INSTALL=false
          ;;
      esac
  done
  shift $((OPTIND-1))

  # get the modulefile full path and basename
  MODULEFILE=${1}

  # strip the module name from the path of the modulefile
  # we'll use this as the target file name.
  MODULENAME=`basename "${MODULEFILE}"`

  shift $((OPTIND-1))

  # and where to put/remove it - the target musy exist and be a directory
  if  [ -e "${1}" ]; then
    if [ -d "${1}" ]; then
      MODULEPATH=${1}
    else
      printf "The given module files directory \"%s\" is not a directory\n" "${1}"
      exit $ERROR_BAD_FILE_SPEC;
    fi
  else
    printf "The given module files directory \"%s\" does not exist\n" "${1}"
  exit $ERROR_PATH_DOES_NOT_EXIST;
  fi
  shift $((OPTIND-1))

  [ "$1" = "--" ] && shift
  
}

main "$@"
