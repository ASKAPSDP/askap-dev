# This is some common stuff used by the scripts in this directory which will source this file before running.
# Some common functions and error codes.

# Errors
  NO_ERROR=0
  ERROR_PATH_DOES_NOT_EXIST=11
  ERROR_BAD_FILE_SPEC=12
  ERROR_BAD_OVERRIDES=64
  ERROR_OVERWRITE_CONFLICT=65
  ERROR_BAD_SVN_TAG=66
  ERROR_TAG_ALREADY_SPECIFIED=67
  ERROR_MISSING_ENV=68
  ERROR_MISSING_CFG_FILE=69
  ERROR_NOT_ASKAPOPS=70
  ERROR_ALREADY_RUNNING=71
  ERROR_WRONG_CLUSTER=72
  ERROR_TIMEOUT=73
  ERROR_BUILD_FAILED=74
  TOLD_TO_DIE=75

# A dev code to identify modules that are dev/testing or any other way not official.
  DEV_MARKER="XHALSDHPOIKA879ASHDKAS987IUHKAD9"

# The Modules system magic cookie. See https://modules.readthedocs.io/en/stable/modulefile.html
  MODULES_COOKIE="#%Module"

# We only want this to run as the asksapops user
doUserCheck () {
  if [[ $USER != "askapops" ]]; then 
    printf "This script must be run as askapops!\n" 
    exit $ERROR_NOT_ASKAPOPS;
  fi 
}
export -f doUserCheck
