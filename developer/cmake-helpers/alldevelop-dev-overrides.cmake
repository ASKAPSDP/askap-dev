# A list of specific version for the dependencies of this build.
#
# The contents here will aid in 'pinning' the contents of the build. Changing this file constitutes changing the build
# and this means that the integration version should be promoted. The version numbers (tags) specified here will be
# injected into the CMakeLists.txt file in the External_project_add definitions and will, therefore, control the
# versions of various sub libs built and integrated. They need not be official versions and can be arbitrary branches or
# tags such as dev feature branches etc.
#
# You can affect the build content by manipulating the "tag/branch" column. Should be something that git expects for that 
# repo.
#
# NOTE:
# This is a special version of this file that has all repos referencing the 'develop' branches.
# Primarily for facilitating an "all develop" build in CI by staging this override file and point $DEV_OVERRIDES to it.

#     Identifier                   tag/branch       cache  type      description                        force it
set ( ASKAP_CMAKE_TAG              develop          CACHE  STRING    "askap.cmake tools"                FORCE )
set ( ASKAP_DEV_TAG                develop          CACHE  STRING    "askap dev tools"                  FORCE )
set ( LOFAR_COMMON_TAG             develop          CACHE  STRING    "lofar-common version"             FORCE )
set ( LOFAR_BLOB_TAG               develop          CACHE  STRING    "lofar-blob version"               FORCE )
set ( BASE_ASKAP_TAG               develop          CACHE  STRING    "base-askap version"               FORCE )
set ( BASE_LOGFILTERS_TAG          develop          CACHE  STRING    "base-logfilters version"          FORCE )
set ( BASE_IMAGEMATH_TAG           develop          CACHE  STRING    "base-imagemath version"           FORCE )
set ( BASE_ASKAPPARALLEL_TAG       develop          CACHE  STRING    "base-askapparallel version"       FORCE )
set ( BASE_SCIMATH_TAG             develop          CACHE  STRING    "base-scimath version"             FORCE )
set ( BASE_ACCESSORS_TAG           develop          CACHE  STRING    "base-accessors version"           FORCE )
set ( BASE_COMPONENTS_TAG          develop          CACHE  STRING    "base-components version"          FORCE )
set ( ASKAP_ANALYSIS_TAG           develop          CACHE  STRING    "askap-analysis version"           FORCE )
set ( ASKAP_YANDASOFT_TAG          develop          CACHE  STRING    "yandasoft version"                FORCE )
set ( ASKAP_PIPELINETASKS_TAG      develop          CACHE  STRING    "askap-pipelinetasks version"      FORCE )
set ( ASKAP_INTERFACES_TAG         develop          CACHE  STRING    "askap-interfaces version"         FORCE )
set ( ASKAP_SMS_TAG                develop          CACHE  STRING    "askap-sms version"                FORCE )
