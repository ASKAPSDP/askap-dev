# Recipe for deploying the containerised ASKAPsoft package on Setonix

Wasim Raja 
08 September 2022

This is a guide on how to deploy askap-askapsoft container on setonix. The guide is intended to help release-manager adopt the recipe for automated deployment of released versions of the software. We assume that the docker container for the release has been built and is available for download/pull. 

1.	Create the software bin directory ensuring read permission for group **askaprt**: 
```
$> tag=1.8.0
$> appDir=/software/projects/askaprt/askapsoft/${tag}
$> mkdir -p ${appDir}/bin
$> cd ${appDir}/bin
```
2.	Pull the docker container: 
```
$> singularity pull docker://csirocass/askapsoft:1.8.0-casacore3.5.0-galaxy
```

3.	Create a wrapper script (I am calling it `generic_wrapper.sh`) in the `${appDir}/bin` area that abstracts away any singularity-specific execution directives for the apps in the container: 
```
#!/bin/bash
image_dir="$(dirname $0)"
image_name=" askapsoft_1.8.0-casacore3.5.0-galaxy.sif "
cmd="$(basename $0)"
args="$@"
singularity exec -B /usr/lib64 $image_dir/$image_name $cmd $args
```

4.	Create soft links (with name = app-name) to this generic_wrapper script:
```
#!/bin/bash -l 
tag=”1.8.0”
imagePath="/software/projects/askaprt/askapsoft/${tag}"
singularityImage=”${imagePath}/bin/askapsoft_${tag}-casacore3.5.0-galaxy.sif”
cd ${imagePath}
appList="$(singularity exec ${singularityImage} ls /usr/local/bin)"
for apps in ${appList}
do
    ln -s generic_wrapper.sh ${apps}
done
```

5.	Now create the modulefile
```
#%Module1.0#####################################################################
##
## askapsoft modulefile
##
## This module definition is versioned and the cmake var identifier '@'VERSION'@'
## will be replaced with the pipeline version identifier at build time.
##
proc ModulesHelp { } {
        global version

        puts stderr "\tThis module provides the ASKAPsoft software package."
}

# No two versions of this module can be loaded simultaneously
conflict        askapsoft
module-whatis   "ASKAPsoft software package"

set ASKAP_HOME  /software/projects/askaprt/askapsoft/1.8.0
prepend-path    PATH            $ASKAP_HOME/bin

setenv ASKAPSOFT_RELEASE 1.8.0
```

