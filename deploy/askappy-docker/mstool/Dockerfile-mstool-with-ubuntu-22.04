# syntax=docker/dockerfile:1
ARG TARGETPLATFORM=linux/amd64
FROM --platform=${TARGETPLATFORM} wasimraja81/askappy-ubuntu-22.04:base-mpich

ARG TARGETPLATFORM

WORKDIR /

# What version/branch of your repo do you want to build this container from?
ARG REPO_NAME=mstool
ARG REPO_VERSION=2.1

# Record useful metadata: See https://docs.docker.com/config/labels-custom-metadata/
LABEL repo.name="${REPO_NAME}" \
      repo.version="${REPO_VERSION}" \
      repo.maintainer.name="Wasim Raja" \
      repo.maintainer.email="Wasim.Raja@csiro.au" \
      repo.description="The image provides python tools \
needed by ASKAPpipeline to run mstool. The image is built on a base image \
built on ubuntu and having casacore and python-casacore." 

ARG BUILD_DIR=/_build
ARG INSTALL_DIR=/_install

# Set default python to python3:
RUN update-alternatives --remove python /usr/bin/python2 \
      && update-alternatives --install /usr/bin/python python /usr/bin/python3 10 \
      && python3 -m pip install -U --force-reinstall pip \
      && pip3 install setuptools

# You may want to define the repo Ver as an environment variable:
ENV MSTOOL_VERSION="${REPO_NAME}/${REPO_VERSION}"

# TODO: Ensure Security: 
RUN mkdir -p /root/.ssh 
COPY id_rsa /root/.ssh/id_rsa
COPY known_hosts /root/.ssh/known_hosts

# Add MSTOOL: 
RUN mkdir -p ${BUILD_DIR}/${REPO_NAME} \
    && git clone ssh://git@bitbucket.csiro.au:7999/~raj030/${REPO_NAME}.git ${BUILD_DIR}/${REPO_NAME} \
    && cd ${BUILD_DIR}/${REPO_NAME} \
    && git fetch --all --tags \
    && git checkout tags/${REPO_VERSION} -b ${REPO_VERSION} \
    && pip3 install -e .

RUN rm -rf /root/.ssh/*
ENV LD_LIBRARY_PATH="/usr/local/lib"
COPY Dockerfile-mstool-with-ubuntu-22.04 ${BUILD_DIR}/${REPO_NAME}/Dockerfile
CMD ["/bin/bash"]

