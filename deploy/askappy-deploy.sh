#!/bin/bash -l
#
# Build and deploy the askappy virtual environment.
#
# This creates and activates a virtual environment and installs all askap python modules and dependencies in it.
# Run this script:
#
#    askap-dev>$ . deploy/askappy-deploy.sh _build _install
#
# The install dir, _install (or where ever), must exist, the build dir, _build (or where ever) will be created if it doesn't exist.
#
# Note: there needs to be some checking and parameter handling added and also versioning is not handled.
#
# Author: Matt Whiting, Eric Bastholm
#

INVOKE_DIR=$(pwd)

module purge
module load python

# This will need doing proper later and also versioning
BUILD_DIR=$1
if [ ! -d "${BUILD_DIR}" ]; then
    mkdir "${BUILD_DIR}"
fi
ORIG_TMP=${TMPDIR}
if [ ! -d "${BUILD_DIR}/tmp" ]; then
    mkdir "${BUILD_DIR}/tmp"
fi
export TMPDIR="${BUILD_DIR}/tmp"

INSTALL_DIR=$2
if [ ! -d "${INSTALL_DIR}" ]; then
    echo "${INSTALL_DIR} does not exist!"
    exit 1
fi

cd ${INSTALL_DIR}
python -m venv askappy --prompt="(askap)"
. askappy/bin/activate
export PYTHONPATH=`python -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())"`
pip install --upgrade pip
pip install setuptools --upgrade

pip install numpy
pip install boost
pip install astropy
pip install matplotlib
pip install pillow
pip install scipy
pip install python-casacore
pip install radio_beam
pip install spectral_cube
pip install aplpy
pip install statsmodels
pip install seaborn
pip install healpy
pip install astroquery
pip install h5py
pip install docopt
pip install mpld3

cd ${INVOKE_DIR}

# Make a dir for python-askap and clone the repo. 
# The --separate-git-dir is to get around the issue on lustre where the .git files have permissions issues due to xattrs. 
# This prevents pip from installing correctly. The .git dir will be put in a tmp area and then pip install seems to ignore it. 
# All git commands will work as normal though.
if [ ! -d "${BUILD_DIR}/python-askap" ]; then
    mkdir ${BUILD_DIR}/python-askap
    git clone --separate-git-dir ${BUILD_DIR}/tmp/python-askap https://bitbucket.csiro.au/scm/tos/python-askap.git ${BUILD_DIR}/python-askap
fi
pip install ${BUILD_DIR}/python-askap

if [ ! -d "${BUILD_DIR}/python-parset" ]; then
    mkdir ${BUILD_DIR}/python-parset
    git clone --separate-git-dir ${BUILD_DIR}/tmp/python-parset https://bitbucket.csiro.au/scm/tos/python-parset ${BUILD_DIR}/python-parset
fi
pip install ${BUILD_DIR}/python-parset

if [ ! -d "${BUILD_DIR}/askap-interfaces" ]; then
    mkdir ${BUILD_DIR}/askap-interfaces
    git clone --separate-git-dir ${BUILD_DIR}/tmp/askap-interfaces https://bitbucket.csiro.au/scm/tos/askap-interfaces ${BUILD_DIR}/askap-interfaces
fi
if [ ! -d "${BUILD_DIR}/python-askap-interfaces" ]; then
    mkdir ${BUILD_DIR}/python-askap-interfaces
    git clone --separate-git-dir ${BUILD_DIR}/tmp/python-askap-interfaces https://bitbucket.csiro.au/scm/tos/python-askap-interfaces ${BUILD_DIR}/python-askap-interfaces
fi
export SLICE_DIR=${INVOKE_DIR}/${BUILD_DIR}/askap-interfaces 
pip install ${BUILD_DIR}/python-askap-interfaces

if [ ! -d "${BUILD_DIR}/python-iceutils" ]; then
    mkdir ${BUILD_DIR}/python-iceutils
    git clone --separate-git-dir ${BUILD_DIR}/tmp/python-iceutils https://bitbucket.csiro.au/scm/tos/python-iceutils ${BUILD_DIR}/python-iceutils
fi
pip install ${BUILD_DIR}/python-iceutils

if [ ! -d "${BUILD_DIR}/python-askap-cli" ]; then
    mkdir ${BUILD_DIR}/python-askap-cli
    git clone --separate-git-dir ${BUILD_DIR}/tmp/python-askap-cli https://bitbucket.csiro.au/scm/tos/python-askap-cli.git ${BUILD_DIR}/python-askap-cli
fi
pip install ${BUILD_DIR}/python-askap-cli

# We'll need the ICE connection config
cp config/tos-ice.cfg ${INSTALL_DIR}/askappy

# And a module file ...
mkdir ${INSTALL_DIR}/modulefiles
cp config/modulefiles/askappy.moddef ${INSTALL_DIR}/modulefiles/askappy

# Cleanup
export TMPDIR=${ORIG_TMP}
